import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'dart:io';
void main() {
 runApp(MyApp());
}


class MyApp extends StatelessWidget {
 @override
 Widget build(BuildContext context) {
   return MaterialApp(
     home: FileOperationDemo(),
   );
 }
}


class FileOperationDemo extends StatefulWidget {
 @override
 _FileOperationDemoState createState() => _FileOperationDemoState();
}

class _FileOperationDemoState extends State<FileOperationDemo> {
 final TextEditingController _textEditingController = TextEditingController();

 String filePath=' ';

 @override
 Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
       title: Text('File Operation Demo'),
     ),
     body: Padding(
       padding: const EdgeInsets.all(16.0),
       child: Column(
         mainAxisAlignment: MainAxisAlignment.center,
         crossAxisAlignment: CrossAxisAlignment.center,
         children: <Widget>[
           TextField(
             controller: _textEditingController,
             decoration: InputDecoration(labelText: 'Nhập dữ liệu'),
           ),


           ElevatedButton(
             onPressed: () async {
               final result= await FilePicker.platform.pickFiles(allowMultiple: false);
               if(result==null){
                 return;
               }
               final files=result.files;
               final path=files.first.path.toString();
               final file =File(path);
               filePath=path;
               final fileContent=await file.readAsString();
               print(fileContent);
               setState(() {
                 _textEditingController.text=fileContent;
               });
             },
             child: Text('Mở File'),
           ),


           ElevatedButton(onPressed: () async{
             final file =File(filePath);
             file.writeAsString(_textEditingController.text);
             setState(() {
               _textEditingController.text=' ';
             });
           }, child: Text("Ghi File"))
         ],
       ),
     ),
   );
 }
}
